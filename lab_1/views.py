from django.shortcuts import render
from datetime import *

# Enter your name here
mhs_name = 'Ivan Abdurrahman' # TODO Implement this

# Variable for calculate age
curr_year = int(datetime.now().strftime("%Y"))
birth_date = 1998

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    return curr_year - birth_year
