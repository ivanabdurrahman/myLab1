  // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '161631881238381',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
      FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          render(true);
        }
        else if (response.status === 'not_authorized') {
          render(false);
        }
        else {
          render(false);
        }
    });
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = (loginFlag) => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="fb-profile">' +
            '<img align="left" class="fb-image-lg" src="'+ user.cover.source +'" alt="Cover image"/>' +
            '<img align="left" class="fb-image-profile thumbnail" src="' + user.picture.data.url + '" width="800px" " alt="Profile image"/>' +
            '<div class="fb-profile-text">' +
                '<h1>'+ user.name +'</h1>' +
                '<h3>'+ user.about +'</h3>' +
                '<p>' + user.email + ' - ' + user.gender + '</p>' +
            '</div>' +
          '<br>' +
        '</div>' +

        '<div class=status-form>' +
          '<input id="postInput" type="text" class="post" placeholder="Apa yang Anda pikirkan sekarang?" />' +
          '<button class="postStatus" id="post-button" onclick="postStatus(postInput.value)">Post ke Facebook</button>' +
        '</div>'
        );

        $('#link-navbar-right').html(
          '<li><a href="{% url "lab-4:index" %}"><span class="glyphicon glyphicon-user"></span> Profile</a></li>' +
          '<li><a onclick="facebookLogout()"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>'
        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            var waktu_dibuat = new Date(value.created_time);
            var statusID = value.id.split("_")[1];
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                  '<p> Dibuat pada ' + waktu_dibuat.toLocaleTimeString() +' tanggal: ' +waktu_dibuat.toLocaleDateString() + '</p>' +
                  '<button class="button-delete-feed" onclick="deleteStatus('+statusID+')">delete status</button>' +                  
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<p> Dibuat pada ' + waktu_dibuat.toLocaleTimeString() +' tanggal: ' +waktu_dibuat.toLocaleDateString() + '</p>' +
                  '<button class="button-delete-feed" onclick="deleteStatus('+statusID+')">delete status</button>' +
                  '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h2>' + value.story + '</h2>' +
                  '<p> Pada ' + waktu_dibuat.toLocaleTimeString() +' tanggal: ' +waktu_dibuat.toLocaleDateString() + '</p>' +
                  '<button class="button-delete-feed" onclick="deleteStatus('+statusID+')">delete status</button>' +     
                  '</div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html(
        '<div id="login-section">' +
        '<button class="loginBtn loginBtn--facebook" onclick="facebookLogin()">Login dengan Facebook</button>' +
        '</div>'
      );
      $('#link-navbar-right').html(
        '<li><a href="{% url "lab-4:index" %}"><span class="glyphicon glyphicon-user"></span> Profile</a></li>'
      );
    }
  };

  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response) {
      // handle the response
      console.log(response);
      // parameter true untuk loginFlag
      render(true);
    }, {scope: 'public_profile,user_posts,user_about_me,email,publish_actions'});
    // menambahkan scope untuk user_post dan user_about_me
    // publish_actions untuk post status
  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.logout(function(response) {
      // Person is now logged out
      console.log(response);
      render(false);
   });
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
    // Memanggil ulang fungsi yang sudah didefinisikan
    // lebih kepada konvensi bagaimana menggunakan function untuk menangani proses asynchronous. 
  const getUserData = (fun) => {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=id,name,cover,picture.width(500).height(500),about,email,gender', 'GET', function (response){
      userID = response.id;
      fun(response);
      console.log('Successful login for: ' + response.name);
    });
  };

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    // pastikan sudah login akun facebook
    /* make the API call */
    FB.api( "/me/feed", function (response) {
        if (response && !response.error) {
          /* handle the result */
          fun (response);
        } else {
          console.log('error: ' +response);
        }
      }
    );
  };

  const postFeed = (message) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    FB.api("/me/feed","POST",
        {
          "message": message
        },
        function(response){
          if (response && !response.error) {
            console.log('Successful post status: ' + message);
            render(true); // callback render untuk update status yang dibuat
          }
        });
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
    $('#postInput').val('');
  };

  const deleteStatus = (idStatus) => {
    // Method untuk menghapus suatu status
    // mengambil id status dari tiap feed dan dijadikan
    // parameter untuk menjalankan method ini
    /* make the API call */
    FB.api("/"+userID+"_"+idStatus, "delete", function(response){
      if (response && !response.error) {
        console.log ('success delete status with id: '+idStatus);
        render(true);
      }
    });
  };